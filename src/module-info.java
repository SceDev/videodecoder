import org.scenarium.PluginsSupplier;

import videodecoder.VideoDecoder;

module VideoDecoder {
	uses PluginsSupplier;
	provides PluginsSupplier with VideoDecoder;

	requires Scenarium;
	requires org.bytedeco.ffmpeg.linux.x86_64;
	requires org.bytedeco.ffmpeg.windows.x86_64;
	requires org.bytedeco.javacpp;
	requires org.bytedeco.ffmpeg;
	requires java.desktop;

	exports videodecoder.scenario;
	exports videodecoder.operator;
}