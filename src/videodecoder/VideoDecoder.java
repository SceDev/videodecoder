package videodecoder;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.scenarium.PluginsSupplier;
import org.scenarium.Scenarium;
import org.scenarium.filemanager.scenariomanager.Scenario;

import videodecoder.operator.VideoReader;
import videodecoder.scenario.Video;

public class VideoDecoder implements PluginsSupplier{
	public static void main(String[] args) {
		Scenarium.main(args);
	}
	
	@Override
	public void populateOperators(Consumer<Class<?>> operatorConsumer) {
		operatorConsumer.accept(VideoReader.class);
	}
	
	@Override
	public void populateScenarios(BiConsumer<Class<? extends Scenario>, String[]> scenarioConsumer) {
		scenarioConsumer.accept(Video.class, new Video().getReaderFormatNames());
	}
	
}
