package videodecoder.test;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferDouble;
import java.awt.image.DataBufferFloat;
import java.awt.image.DataBufferInt;
import java.awt.image.DataBufferShort;
import java.awt.image.DataBufferUShort;
import java.io.IOException;
import java.nio.file.AccessDeniedException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;

import org.bytedeco.ffmpeg.global.avutil;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.Java2DFrameConverter;

//0.802 s avec convert
//0.188 s sans
//0.422 s optim
//0.247 s optim sans create image
public class FFmpegTest {

	public static void main(String[] args) throws IOException {
		avutil.av_log_set_level(avutil.AV_LOG_QUIET);
		test(Path.of("/home/revilloud/Dropbox/Mohammed et Marc/image.avi"), true);
		
		
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:**.{avi,mp4}");
		ArrayList<Path> fileList = new ArrayList<>();
		Files.walkFileTree(Path.of("/home/revilloud/Dropbox/"), new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (matcher.matches(file))
					fileList.add(file);
				return super.visitFile(file, attrs);
			}

			@Override
			public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
				if (exc instanceof AccessDeniedException) {
					return FileVisitResult.SKIP_SUBTREE;
				}

				return super.visitFileFailed(file, exc);
			}
		});
		System.out.println("Nb video: " + fileList.size());
		fileList.parallelStream().forEach(p -> test(p, false));
		System.out.println("end of process");
	}


	private static void test(Path path, boolean showInfo) {
		System.out.println("Test of video: " + path);
		try (FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(path.toString())){
			BufferedImage tmpImg = null;
			Java2DFrameConverter converter = new Java2DFrameConverter();
			grabber.start();
			int it = 0;
			int it2 = 0;
			while (true) {
				Frame source = grabber.grab();
				if (source == null)
					break;
				it2++;
				BufferedImage img = converter.convert(source, tmpImg);
				if (img != null) {
					tmpImg = img;
					BufferedImage img2 = converter.getBufferedImage(source, 1);
					if (!compareImages(img, img2))
						System.err.println("Error when comparing frames with video: " + path);
//					try {
//						ImageIO.write(img, "BMP", new File(it + ".bmp"));
//					} catch (IOException e) {
//						e.printStackTrace();
//					}
					it++;
				}
			}
			System.out.println("Test ok: " + path + " LengthInFrames: " + grabber.getLengthInFrames() + " nbFrames: " + it);
			if (showInfo) {
				System.out.println("FrameRate: " + grabber.getFrameRate());
				System.out.println("FrameNumber: " + grabber.getFrameNumber());
				System.out.println("End time: " + grabber.getLengthInTime() / 1000);
				System.out.println("LengthInVideoFrames: " + grabber.getLengthInVideoFrames());
				System.out.println("Number of frames: " + it2);
				System.out.println("Number of image frames: " + it);
				System.out.println("LengthInFrames: " + grabber.getLengthInFrames());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static boolean compareImages(BufferedImage imgA, BufferedImage imgB) {
		if (imgA.getWidth() != imgB.getWidth() || imgA.getHeight() != imgB.getHeight() || imgA.getType() != imgB.getType())
			return false;
		DataBuffer buffA = imgA.getData().getDataBuffer();
		DataBuffer buffB = imgA.getData().getDataBuffer();
		if (buffA instanceof DataBufferByte) {
			if (!(buffB instanceof DataBufferByte))
				return false;
			return Arrays.equals(((DataBufferByte) buffA).getData(), ((DataBufferByte) buffB).getData());
		} else if (buffA instanceof DataBufferDouble) {
			if (!(buffB instanceof DataBufferDouble))
				return false;
			return Arrays.equals(((DataBufferDouble) buffA).getData(), ((DataBufferDouble) buffB).getData());
		} else if (buffA instanceof DataBufferDouble) {
			if (!(buffB instanceof DataBufferDouble))
				return false;
			return Arrays.equals(((DataBufferDouble) buffA).getData(), ((DataBufferDouble) buffB).getData());
		} else if (buffA instanceof DataBufferFloat) {
			if (!(buffB instanceof DataBufferFloat))
				return false;
			return Arrays.equals(((DataBufferFloat) buffA).getData(), ((DataBufferFloat) buffB).getData());
		} else if (buffA instanceof DataBufferInt) {
			if (!(buffB instanceof DataBufferInt))
				return false;
			return Arrays.equals(((DataBufferInt) buffA).getData(), ((DataBufferInt) buffB).getData());
		} else if (buffA instanceof DataBufferShort) {
			if (!(buffB instanceof DataBufferShort))
				return false;
			return Arrays.equals(((DataBufferShort) buffA).getData(), ((DataBufferShort) buffB).getData());
		} else if (buffA instanceof DataBufferUShort) {
			if (!(buffB instanceof DataBufferUShort))
				return false;
			return Arrays.equals(((DataBufferUShort) buffA).getData(), ((DataBufferUShort) buffB).getData());
		} else
			return false;
	}
}