package videodecoder.scenario;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.beanmanager.editors.DynamicVisibleBean;
import org.bytedeco.ffmpeg.global.avutil;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.scenarium.filemanager.scenariomanager.ScenarioException;
import org.scenarium.filemanager.scenariomanager.TimedScenario;
import org.scenarium.timescheduler.Scheduler;

public class Video extends TimedScenario implements DynamicVisibleBean {
	private static String[] extension = new String[] { "gsm", "wsd", "vqf", "vqe", "bit", "vql", "aa3", "scc", "fap", "mov", "ape", "mpl2", "yop", "apl", "lvf", "mxg", "vag", "3g2", "mpa", "aa",
			"xmv", "mpc", "pvf", "viv", "sdr2", "vc1", "cdg", "genh", "stl", "al", "mpo", "c2", "cdata", "str", "fsb", "mj2", "sln", "h261", "h264", "avs2", "h265", "cdxl", "musx", "mtaf", "sub",
			"aac", "oma", "rt", "rsd", "sds", "aqt", "brstm", "sdx", "flac", "ty+", "omg", "nsp", "mac", "smi", "kux", "rso", "sb", "sup", "ac3", "aix", "sf", "msbc", "hevc", "3gp", "rco", "sami",
			"ser", "j2k", "art", "mjpeg", "sw", "rcv", "flm", "mjpg", "nist", "h26l", "264", "asc", "flv", "265", "txt", "thd", "302", "v", "vtt", "dif", "msf", "g723_1", "mka", "ast", "cgi", "ty",
			"acm", "bmv", "ice", "nut", "ub", "ogg", "dv", "son", "paf", "mks", "diz", "ul", "dav", "mkv", "yuv10", "qcif", "mk3d", "xvag", "m2a", "tak", "adf", "uw", "dtshd", "idf", "sph", "adp",
			"vb", "v210", "mlp", "ads", "adx", "nfo", "722", "pjs", "shn", "aea", "idx", "vt", "cif", "dss", "aptxhd", "rgb", "avc", "ss2", "avi", "bcstm", "g729", "dtk", "m4a", "yuv", "avr", "aptx",
			"afc", "avs", "g722", "tta", "tco", "mvi", "daud", "dts", "eac3", "ivr", "ans", "m4v", "mp2", "mp4", "vpk", "mp3", "y4m", "xl", "sbc", "ifv", "sbg", "ircam", "bfstm", "svag" };
	private FFmpegFrameGrabber grabber;
	private Java2DFrameConverter converter;
	private long previousTimePointer = -1;
	private int nbFrame = -1;
	private double frameRate = -1;

	static {
		avutil.av_log_set_level(avutil.AV_LOG_QUIET);
	}
	
	public static void main(String[] args) throws IOException {
		String ffmpeg = Loader.load(org.bytedeco.ffmpeg.ffmpeg.class);
		ProcessBuilder pb = new ProcessBuilder(ffmpeg, "-demuxers");
		Process p = pb.start();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		StringBuilder builder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append(System.getProperty("line.separator"));
		}
		ArrayList<String> demuxers = new ArrayList<>();
		String[] results = builder.toString().split("\\r?\\n|\\r");
		int i = 0;
		while(i < results.length && !results[i++].trim().equals("--"));
		for (; i < results.length; i++)
			demuxers.addAll(Arrays.asList(results[i].trim().split("\\s+")[1].split(",")));
		HashSet<String> extension = new HashSet<>();
		String extKeyWord = "Common extensions:";
		for (String demuxer : demuxers) {
			pb = new ProcessBuilder(ffmpeg, "-h", "demuxer=" + demuxer);
			p = pb.start();
			reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			builder = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				builder.append(line);
				builder.append(System.getProperty("line.separator"));
			}
			results = builder.toString().split("\\r?\\n|\\r");
			for (i = 0; i < results.length; i++) {
				line = results[i].trim();
				int indexOfExt = line.indexOf(extKeyWord);
				if(indexOfExt != -1) {
					line = line.substring(indexOfExt + extKeyWord.length()).trim();
					if(line.endsWith("."))
						line = line.substring(0, line.length() - 1);
					extension.addAll(Arrays.asList(line.split(",")));
				}
			}
		}
		System.out.println("private static String[] extension = new String[]{\"" + extension.stream().collect(Collectors.joining("\",\"")) + "\"};");
	}

	@Override
	public boolean canCreateDefault() {
		return false;
	}

	@Override
	public void load(File scenarioFile, boolean backgroundLoading) throws IOException, ScenarioException {
		grabber = new FFmpegFrameGrabber(scenarioFile.getAbsolutePath());
		converter = new Java2DFrameConverter();
		previousTimePointer = -Integer.MAX_VALUE;
		grabber.start();
		updateFrameRateAndNbFrame();
		process((long) 0);
		fireLoadChanged();
	}

	@Override
	protected boolean close() {
		super.close();
		boolean success = false;
		if (grabber != null)
			try {
				grabber.close();
				success = true;
			} catch (org.bytedeco.javacv.FrameGrabber.Exception e) {
				e.printStackTrace();
			}
		grabber = null;
		converter = null;
		nbFrame = -1;
		frameRate = -1;
		return success;
	}

	@Override
	public long getBeginningTime() {
		return 0;
	}
	
	@Override
	public long getNbFrame() {
		updateFrameRateAndNbFrame();
		return nbFrame;
	}

//	@Override
//	public long getEndTime() {
//		updateFrameRateAndNbFrame();
//		long endTime = (long) (1000 / frameRate * nbFrame);
//		return endTime < 0 ? 1 : endTime;
//	}

	public double getFrameRate() {
		updateFrameRateAndNbFrame();
		return frameRate;
	}

	private void updateFrameRateAndNbFrame() {
		if (nbFrame == -1) {
			if (grabber == null) {
				if (file != null && file.exists())
					try (FFmpegFrameGrabber _grabber = new FFmpegFrameGrabber(file.getAbsolutePath())){
						_grabber.start();
						nbFrame = _grabber.getLengthInVideoFrames();
						frameRate = _grabber.getFrameRate();
						_grabber.stop();
					} catch (IOException e) {}
			} else {
				nbFrame = grabber.getLengthInVideoFrames();
				frameRate = grabber.getFrameRate();
			}
			setPeriod(frameRate == -1 ? -1 : 1000 / frameRate);
		}
	}

	@Override
	public void setFile(File file) {
		if (Objects.equals(file, this.file))
			return;
		nbFrame = -1;
		frameRate = -1;
		setPeriod(1);
		super.setFile(file);
	}

	@Override
	public Class<?> getDataType() {
		return BufferedImage.class;
	}

	@Override
	//Voir avec steve pour mkv
	//https://stackoverflow.com/questions/50069235/what-are-all-of-the-file-extensions-supported-by-ffmpeg
	public String[] getReaderFormatNames() {
		return extension;
	}

	@Override
	public int getSchedulerType() {
		return Scheduler.TIMER_SCHEDULER;
	}

	@Override
	public void populateInfo(LinkedHashMap<String, String> info) throws IOException {
		info.put("Format", grabber.getFormat());
		info.put("SensorPattern", Double.toString(grabber.getSensorPattern()));
		info.put("FrameRate", Double.toString(grabber.getFrameRate()));
		info.put("SampleMode", grabber.getSampleMode().toString());
		info.put("SampleFormat", Double.toString(grabber.getSampleFormat()));
		info.put("SampleRate", Double.toString(grabber.getSampleRate()));
		info.put("TriggerMode", Boolean.toString(grabber.isTriggerMode()));
		info.put("Timeout", Double.toString(grabber.getTimeout()));
		info.put("NumBuffers", Double.toString(grabber.getNumBuffers()));
		info.put("FrameNumber", Double.toString(grabber.getFrameNumber()));
		info.put("MaxDelay", Double.toString(grabber.getMaxDelay()));
		try {
			info.put("DelayedTime", Double.toString(grabber.getDelayedTime()));
		} catch (InterruptedException e) {} catch (ExecutionException e) {}
		info.put("LengthInTime", Double.toString(grabber.getLengthInTime()));
		info.put("LengthInFrames", Double.toString(grabber.getLengthInFrames()));
		if (grabber.hasVideo()) {
			info.put("VideoStream", Integer.toString(grabber.getVideoStream()));
			info.put("LengthInVideoFrames", Double.toString(grabber.getLengthInVideoFrames()));
			info.put("VideoCodec", Double.toString(grabber.getVideoCodec()));
			if (grabber.getVideoCodecName() != null)
				info.put("VideoCodecName", grabber.getVideoCodecName());
			info.put("ImageWidth", Double.toString(grabber.getImageWidth()));
			info.put("ImageHeight", Double.toString(grabber.getImageHeight()));
			info.put("ImageMode", grabber.getImageMode().toString());
			info.put("PixelFormat", Double.toString(grabber.getPixelFormat()));
			info.put("VideoFrameRate", Double.toString(grabber.getVideoFrameRate()));
			info.put("VideoBitrate", Double.toString(grabber.getVideoBitrate()));
			info.put("ImageScalingFlags", Double.toString(grabber.getImageScalingFlags()));
			info.put("AspectRatio", Double.toString(grabber.getAspectRatio()));
			info.put("BitsPerPixel", Double.toString(grabber.getBitsPerPixel()));
			info.put("Gamma", Double.toString(grabber.getGamma()));
			info.put("Deinterlace", Boolean.toString(grabber.isDeinterlace()));
		}
		if (grabber.hasAudio()) {
			info.put("AudioStream", Double.toString(grabber.getAudioStream()));
			info.put("LengthInAudioFrames", Double.toString(grabber.getLengthInAudioFrames()));
			info.put("AudioCodec", Double.toString(grabber.getAudioCodec()));
			if (grabber.getAudioCodecName() != null)
				info.put("AudioCodecName", grabber.getAudioCodecName());
			info.put("AudioChannels", Double.toString(grabber.getAudioChannels()));
			info.put("AudioFrameRate", Double.toString(grabber.getAudioFrameRate()));
			info.put("AudioBitrate", Double.toString(grabber.getAudioBitrate()));
		}
		info.putAll(grabber.getMetadata());
	}

	@Override
	public void process(Long timePointer) throws Exception {
		double period = getPeriod();
		if (timePointer <= period || Math.abs(timePointer - (previousTimePointer + period)) > period / 2)
			grabber.setTimestamp((long) ((timePointer / period) * 1_000_000.0 / frameRate));
		while (true) {
			Frame source = grabber.grab();
			if (source == null)
				break;
			BufferedImage img = converter.convert(source, (BufferedImage) scenarioData);
			if (img != null) {
				scenarioData = img;
				break;
			}
		}
		previousTimePointer = timePointer;
	}

	@Override
	public void save(File file) {}

	@Override
	public void setVisible() {
		super.setVisible();
		fireSetPropertyVisible(this, "period", false);
	}
}