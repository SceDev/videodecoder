package videodecoder.operator;

import java.awt.image.BufferedImage;
import java.util.function.Consumer;

import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber.Exception;
import org.bytedeco.javacv.Java2DFrameConverter;
import org.scenarium.filemanager.scenario.dataflowdiagram.EvolvedOperator;

public class VideoReader extends EvolvedOperator {
	private String devicePath = "rtsp://wowzaec2demo.streamlock.net/vod/mp4:BigBuckBunny_115k.mov";
	private GrabberThread grabberThread;

	public void birth() {
		onStart(() -> this.grabberThread = new GrabberThread(devicePath, this::triggerOutput));
	}

	public BufferedImage process() {
		return null;
	}

	public void death() {
		Thread grabberThread = this.grabberThread;
		if (grabberThread != null) {
			grabberThread.interrupt();
			grabberThread = null;
		}
	}

	public String getDevicePath() {
		return devicePath;
	}

	public void setDevicePath(String devicePath) {
		this.devicePath = devicePath;
		restartLater();
	}
}

class GrabberThread extends Thread {
	private Consumer<Object> consumer;
	private final String devicePath;

	public GrabberThread(String devicePath, Consumer<Object> consumer) {
		this.devicePath = devicePath;
		this.consumer = consumer;
		start();
	}

	@Override
	public void run() {
		if(devicePath == null || devicePath.isEmpty())
			return;
		try (FFmpegFrameGrabber grabber = new FFmpegFrameGrabber(devicePath)){
			grabber.start();
			Java2DFrameConverter converter = new Java2DFrameConverter();
			BufferedImage imgBuffer = null;
			while (!isInterrupted()) {
				try {
					Frame source = grabber.grab();
					if (source != null) {
						BufferedImage img = converter.convert(source, (BufferedImage) imgBuffer);
						if(img != null)
							this.consumer.accept(img);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
}
